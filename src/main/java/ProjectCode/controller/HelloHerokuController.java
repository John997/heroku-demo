package ProjectCode.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ProjectCode.model.HerokuDemoUser;
import ProjectCode.service.HerokuDemoService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class HelloHerokuController {

	private final HerokuDemoService herokuDemoService;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<HerokuDemoUser>> home() {
		List<HerokuDemoUser> herokuDemoUser = herokuDemoService.findAll();
		return  new ResponseEntity<List<HerokuDemoUser>>(herokuDemoUser, HttpStatus.OK);
	}

}
