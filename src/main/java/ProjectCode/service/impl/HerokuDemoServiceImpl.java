package ProjectCode.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import ProjectCode.model.HerokuDemoUser;
import ProjectCode.repository.HerokuDemoRepository;
import ProjectCode.service.HerokuDemoService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class HerokuDemoServiceImpl implements HerokuDemoService {

	public final  HerokuDemoRepository herokuDemoRepository;

	@Override
	public List<HerokuDemoUser> findAll() {
		
		return herokuDemoRepository.findAll();
	}
}
