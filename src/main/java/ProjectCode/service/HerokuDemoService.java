package ProjectCode.service;

import java.util.List;

import ProjectCode.model.HerokuDemoUser;

public interface HerokuDemoService {
	
	List<HerokuDemoUser> findAll();
}
