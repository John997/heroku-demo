package ProjectCode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ProjectCode.model.HerokuDemoUser;

@Repository
public interface HerokuDemoRepository extends JpaRepository<HerokuDemoUser, Integer> {
	
	

}
