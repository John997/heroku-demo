FROM openjdk:11-jre-slim
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
CMD [ "sh", "-c", "java -Dserver.port=$PORT \
-Dspring.datasource.url=$SPRING_DATASOURCE_URL \
-Dspring.datasource.username=$SPRING_DATASOURCE_USERNAME \
-Dspring.datasource.password=$SPRING_DATASOURCE_PASSWORD -Xmx300m -Xss512k -XX:CICompilerCount=2 -Dfile.encoding=UTF-8 -XX:+UseContainerSupport -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
